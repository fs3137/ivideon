#include <iostream>
#include <string>
#include <sys/stat.h> // mkfifo
#include "led.h"



using namespace std;
const char* pipe_input = "/tmp/input.pipe";
const char* pipe_output = "/tmp/";



void Output(string id, string output) {
	if(id.size() == 0 || output.size() == 0) {
		cout << "Empty output" << endl;
		return (void)0;
	}
	string filename = pipe_output + id + ".pipe";
	FILE* f = fopen (filename.c_str(), "w");
	if (f != NULL) {
		fputs (output.c_str(), f);
		fclose (f);
	}
	else {
		cout << "Error: open(write)" << endl;
	}
}




int main() {
	LED led(0, false, 0);
	char* buffer;
	size_t len = 0;
	ssize_t read;
	mkfifo(pipe_input, 0666);
	FILE* fifo = fopen(pipe_input, "r");
	if(fifo < 0) {
		cout << "Error: open(read)" << endl;
		return 0;
	}
	while(1) {
		while ((read = getline(&buffer, &len, fifo)) != -1) {
			cout << "=====================================================" << endl;
			cout << "Input:\n" << buffer << endl;
			pair<string, string> result = led.SendCommand(buffer);
			Output(result.first, result.second);
			cout << "Output for " << result.first << ":\n" << result.second << endl;
		}
		sleep(1);
	}
	fclose(fifo);
	return 0;
}
