#!/bin/bash
echo "Client 1.0"



#Params
input_pipe="/tmp/input.pipe"
output_pipe="/tmp/$$.pipe"



# Help
usage="Usage:
$0 ON
$0 OFF
$0 STATE
$0 RED
$0 GREEN
$0 BLUE
$0 COLOR
$0 RATE
$0 NEWRATE 0-5"



# Commands
declare -A commands
commands["ON"]="set-led-state on"
commands["OFF"]="set-led-state off"
commands["STATE"]="get-led-state"
commands["RED"]="set-led-color red"
commands["GREEN"]="set-led-color green"
commands["BLUE"]="set-led-color blue"
commands["COLOR"]="get-led-color"
commands["NEWRATE"]="set-led-rate $2"
commands["RATE"]="get-led-rate"



#Input
if [[ "$1" =~ ^$ ]] || [[ ${commands["$1"]} =~ ^$ ]]; then
  echo "$usage"
  exit
fi



# Проверка существования пайпов
if [ ! -p $input_pipe ]; then
   mkfifo $input_pipe
fi
if [ ! -p $output_pipe ]; then
   mkfifo $output_pipe
fi



# Request
echo "Waiting answer from camera."
echo "$$ ${commands["$1"]}" > $input_pipe
cat $output_pipe
echo
rm $output_pipe
