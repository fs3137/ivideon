#include "led.h"
#define COLORS_NUMBER 3
string gColors[COLORS_NUMBER] = { "red", "blue", "green"};



LED::LED(char color, bool state, char rate):
color_(color), state_(state), rate_(rate) {}



string LED::GetColor() const {
	string str = "OK " + gColors[color_];
	return str;
}



string LED::GetState() const {
	return state_ ? "OK on": "OK off";
}



string LED::GetRate() const {
	string str = "OK ";
	str += (char)('0' + rate_);
	return str;
}



string LED::SetColor(string color) {
	for(int i = 0; i < COLORS_NUMBER; i++) {
		if(color == gColors[i]) {
			color_ = i;
			return "OK";
		}
	}
	return "FAILED";
}



string LED::SetState(string state) {
	if(state == "on") {
		state_ = true;
		return "OK";
	}
	if(state == "off") {
		state_ = false;
		return "OK";
	}
	return "FAILED";
}



string LED::SetRate(string rate) {
	if(rate.length() != 1 || rate[0] < '0' || rate[0] > '5') return "FAILED";
	rate_ = stoi(rate);
	return "OK";
}



/*
 * Выполняет команду и возвращает пару <ID, Ответ>
 * ID используется для доставки ответа тому клиенту, который послал команду.
 */
pair<string, string> LED::SendCommand(string command) {
	vector<string> params;
	pair<string, string> result;
	boost::trim(command);
	boost::split(params, command, boost::is_any_of(" "));
	switch(params.size()) {
		case(2):
			if(params[1] == "get-led-state") result.second = GetState();
			else if(params[1] == "get-led-color") result.second = GetColor();
			else if(params[1] == "get-led-rate") result.second = GetRate();
			break;
		case(3):
			if(params[1] == "set-led-state") result.second = SetState(params[2]);
			else if(params[1] == "set-led-color") result.second = SetColor(params[2]);
			else if(params[1] == "set-led-rate") result.second = SetRate(params[2]);
			break;	
	}
	if(params.size() > 0) {
		result.first = params[0];
		if(result.second.size() == 0) result.second = "FAILED";
	}
	return result;
} 
