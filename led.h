#pragma once
#include <iostream>
#include <string>
#include <vector>
#include <boost/algorithm/string.hpp> // trim and split
using namespace std;



class LED {
	private:
		char color_;
		bool state_;
		char rate_;
	public:
		LED(char, bool, char);
		string SetColor(string);
		string SetState(string);
		string SetRate(string);
		string GetColor() const;
		string GetState() const;
		string GetRate() const;
		pair<string, string> SendCommand(string);
};
